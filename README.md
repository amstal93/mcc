Table of Contents
=================

* [Table of Contents](#table-of-contents)
* [[WIP] Container with various toolsets and my own dotfiles](#wip-container-with-various-toolsets-and-my-own-dotfiles)
* [How to run this on your machine](#how-to-run-this-on-your-machine)
* [Notes](#notes)
* [Issues](#issues)
* [Todo](#todo)
* [Pipeline Configuration](#pipeline-configuration)
   * [Gitlab CI](#gitlab-ci)
   * [Github Actions](#github-actions)
* [Tools installed](#tools-installed)
   * [network utils](#network-utils)
   * [shell utils](#shell-utils)
   * [dev utils](#dev-utils)
   * [kubernetes](#kubernetes)
   * [cloud tools](#cloud-tools)
   * [Custom dotfiles for :](#custom-dotfiles-for-)
   * [Tools used](#tools-used)
      * [asdf plugins](#asdf-plugins)
      * [asdf packages](#asdf-packages)
      * [Pip Packages](#pip-packages)
* [If you want to tweak this with your personal conf](#if-you-want-to-tweak-this-with-your-personal-conf)
* [Links of tools used/installed](#links-of-tools-usedinstalled)
* [Repo Tree](#repo-tree)

Created by [gh-md-toc](https://github.com/ekalinin/github-markdown-toc)
# [WIP] Container with various toolsets and my own dotfiles
Used for testing new clusters based on Ubuntu:22.04
Feel free to fork and modify as you please
# How to run this on your machine
By running this 
```shell
docker run -ti registry.gitlab.com/frankper/mcc
```
# Notes
* User created for the container has passwordless sudo
# Issues 
* Large image size (>0.8gb)
# Todo 
* Add tests
* Clean up Krew Addons
* Add SSH,GPG,AWS,KUBECONFIG functionality
* Correct typos in README.md as I am sure there are many :) 
* Add github actions configuration
* Reduce image size if possible 
* Reduce container image build times (currently >15 minutes)

# Pipeline Configuration
## Gitlab CI 
Repo contains a gitlab pipeline [file](https://gitlab.com/frankper/mcc/-/blob/main/.gitlab-ci.yml) to build and publish with every push to main
* an image with "commit sha" tag 
* an image with "latest" tag
* an image with "git tag" tag ,if pressent
## Github Actions
WIP (at the moment blocked due to image size limitations on the [free](https://github.com/features/packages#pricing) github registry)
# Tools installed 
## network utils 
* dnsutils arp-scan speedtest-cli sipcalc ipmitool traceroute nmap
## shell utils
* sudo fzf bat wget git curl zsh vim ncdu exa bat fd duf tldr starship tmux direnv asdf
## dev utils
* base-devel git
## kubernetes
* jq yq kubectl fluxctl arkade kubetail eksctl
## cloud tools 
* aws-cli sops eksctl
## Custom dotfiles for :
* spacevim ohMyZsh tmux starship
## Tools used 
### asdf plugins
* tmux argo rust golang helm awscli direnv yarn kubectl 1password-cli eksctl kustomize k9s docker-compose krew kubetail nodejs pulumi kind hugo boundary consul nomad packer sentinel serf terraform vault waypoint
### asdf packages
* kubetail

# If you want to tweak this with your personal conf
* zsh config is [here](https://gitlab.com/frankper/mcc/-/tree/main/utils/dotfiles/zsh) 
* starship config is [here](https://gitlab.com/frankper/mcc/-/tree/main/utils/dotfiles/starship)
* tmux config is pulled from [here](https://github.com/frankperrakis/tmux-config)
* spacevim config is [here](https://gitlab.com/frankper/mcc/-/tree/main/utils/dotfiles/vim/nvim)
* If you want to add more pip packages go [here](https://gitlab.com/frankper/mcc/-/blob/main/utils/scripts/setup_pip.sh)
* If you want to add more pacman/system packages add them [here](https://gitlab.com/frankper/mcc/-/blob/main/utils/scripts/pacman_packages_to_install.txt)
* Add more asdf packages or plugins [here](https://gitlab.com/frankper/mcc/-/blob/main/utils/scripts/setup_asdf.sh) and add global defaults [here](https://gitlab.com/frankper/mcc/-/blob/main/utils/dotfiles/asdf/.tool-verions)
* Add more Kubectl Krew Plugins [here](https://gitlab.com/frankper/mcc/-/blob/main/utils/scripts/install_krew.sh)
* If you want to change aliases set in ZSH do it [here](https://gitlab.com/frankper/mcc/-/blob/main/utils/dotfiles/zsh/.zsh_profile_main)
# Links of tools used/installed
* [Spacevim](spacevim.org/)
* [Starship](https://starship.rs)
* [Arcade](https://github.com/alexellis/arkade)
* [Oh My Zsh](https://ohmyz.sh/)
* [Krew Plugin Manager](https://krew.sigs.k8s.io/)
* [Asdf](https://github.com/asdf-vm/asdf)
* [Direnv](https://direnv.net/)
# Repo Tree
```
├── .dockerignore
├── .gitignore
├── .gitlab-ci.yml
├── Dockerfile
├── LICENSE
├── README.md
└── utils/
   ├── dotfiles/
   │  ├── asdf/
   │  │  └── .tool-versions
   │  ├── bash/
   │  │  ├── .bash_logout
   │  │  ├── .bash_profile
   │  │  └── .bashrc
   │  ├── starship/
   │  │  └── starship.toml
   │  ├── tmux/
   │  ├── tmuxinator/
   │  ├── vim/
   │  │  └── init.toml
   │  └── zsh/
   │     ├── .zsh_history
   │     ├── .zsh_profile_linux
   │     ├── .zsh_profile_main
   │     └── .zshrc
   ├── gpg/
   ├── scripts/
   │  ├── install_arkade.sh
   │  ├── install_asdf.sh
   │  ├── install_go.sh
   │  ├── install_homebrew.sh
   │  ├── install_krew.sh
   │  ├── install_omzsh.sh
   │  ├── install_spacevim.sh
   │  ├── install_tmux_samoskin.sh
   │  ├── pacman_packages_to_install.txt
   │  ├── setup_asdf.sh
   │  ├── setup_pip.sh
   │  └── update_my_system.sh
   └── ssh
```