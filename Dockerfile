FROM ubuntu:22.04

# update packages 
RUN  apt update -y \
&&  apt upgrade -y
# Set the locale
RUN DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt -y install tzdata
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
ENV DEBIAN_FRONTEND noninteractive
# install locales 
RUN apt install locales
RUN locale-gen
# install software 
COPY utils/scripts/apt_packages_to_install.txt .
RUN xargs apt install -y < apt_packages_to_install.txt
# cleanup packages 
RUN apt autoremove -y && apt clean -y \
&& rm -rf /var/cache/apt/archives/

# Create a user with passwordless sudo
ARG USERNAME=user
ARG USER_UID=1000
ARG USER_GID=$USER_UID
RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME \
    && echo $USERNAME ALL=\(root\) NOPASSWD:ALL > /etc/sudoers.d/$USERNAME \
    && echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers \
    && chmod 0440 /etc/sudoers.d/$USERNAME

# copy files into the container 
WORKDIR /home/user
COPY --chown=$USERNAME:$USERNAME . .
# setup user space
USER $USERNAME
# fzf setup
RUN ["/bin/zsh", "-c", "./utils/scripts/install_fzf.sh"]
# omz setup
RUN ["/bin/zsh", "-c", "./utils/scripts/install_omzsh.sh"]
RUN ["/bin/zsh", "-c", "./utils/scripts/install_omz_plugins.sh"]
COPY --chown=$USERNAME:$USERNAME utils/dotfiles/zsh .
# setup asdf
RUN ["/bin/zsh", "-c", "./utils/scripts/install_asdf.sh"]
RUN ["/bin/zsh", "-c", "./utils/scripts/setup_asdf.sh"]
# install latest version of kubectl
RUN ["/bin/zsh", "-c", "./utils/scripts/install_kubectl.sh"]
# install starship
RUN ["/bin/zsh", "-c", "./utils/scripts/install_starship.sh --yes"]
# spacevim , tmux , arkade , krew 
RUN ["/bin/zsh", "-c", "./utils/scripts/install_arkade.sh"]
RUN ["/bin/zsh", "-c", "./utils/scripts/install_tmux_samoskin.sh"]
RUN ["/bin/zsh", "-c", "./utils/scripts/install_krew.sh"]
RUN ["/bin/zsh", "-c", "./utils/scripts/install_spacevim.sh"]
# add dotfiles
COPY --chown=$USERNAME:$USERNAME utils/dotfiles/starship .
COPY --chown=$USERNAME:$USERNAME utils/dotfiles/vim/init.toml /home/user/.SpaceVim.d/init.toml
COPY --chown=$USERNAME:$USERNAME utils/dotfiles/asdf/.tool-versions .

ENTRYPOINT ["/bin/zsh"]
ENV USER=user
